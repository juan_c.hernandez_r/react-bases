// Desestructuracion
// Asignacion desustructurante

const persona = {
  nombre: "Tony",
  edad: 45,
  clave: "Ironman",
  rango: "Soldado",
};

// const { nombre, edad, clave } = persona;
// console.log(nombre);
// console.log(edad);
// console.log(clave);

const retornaContext = ({ clave, nombre, edad, rango = "Capitan" }) => {
  //   const { nombre, edad, clave } = persona;
  //   console.log(nombre, edad, rango);
  return {
    nombreClave: clave,
    anios: edad,
    latlng: {
      lat: 14.5412,
      lng: -14.5412,
    },
  };
};

const {
  nombreClave,
  anios,
  latlng: { lat, lng },
} = retornaContext(persona);

console.log(nombreClave, anios);
console.log(lat);
console.log(lng);
